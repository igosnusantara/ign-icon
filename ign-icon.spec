Summary:IGOS Nusantara ICON THEMES
Name:IGNDelapan
Version:8.2
Release:14
License:GPLv2
Group:System Environment/Base
URL:http://igos-nusantara.or.id
Source0:%{name}.tar.gz
BuildRoot:%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:noarch
Requires:mate-session

%description
IGN ICON THEMES

%prep
%setup -q -n %{name}

%install
make install PREFIX=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%dir 
/usr/share/
%config %attr(0755,root,root) /usr/share/*

%changelog
* Thu Aug 29 2012 Radite Putut <radith412@gmail.com>
- build fix devices categories app status action place icon 8.2
* Thu Aug 6 2012 Radite Putut <radith412@gmail.com>
- build fix devices icon 8.1.1
* Thu Aug 6 2012 Radite Putut <radith412@gmail.com>
- build fix devices icon 8.1
* Thu Aug 5 2012 Radite Putut <radith412@gmail.com>
- build fix devices icon 8.0.5
* Thu Aug 5 2012 Radite Putut <radith412@gmail.com>
- build fix folder devices ign icon 8.0.4
* Thu Aug 2 2012 Radite Putut <radith412@gmail.com>
- build fix devices status ign icon 8.0.3
* Thu Aug 2 2012 Radite Putut <radith412@gmail.com>
- build fix status panel package release ign icon 8.0.2
* Thu Aug 2 2012 Radite Putut <radith412@gmail.com>
- build package release ign icon 8.0.1
* Wed Feb 9 2012 Ibnu Yahya <ibnu.yahya@toroo.org>
- build package

